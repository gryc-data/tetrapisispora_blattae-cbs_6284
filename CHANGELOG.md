# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.1 (2021-05-18)

### Edited

* Build feature hierarchy.

### Fixed

* CDS turned into pseudo (interupted by gap or chromosome end): TBLA_0A00100.
* CDS turned into pseudo (interupted by gap or chromosome end): TBLA_0A09450.
* CDS turned into pseudo (interupted by gap or chromosome end): TBLA_0B08195.
* Missing stop codon (fixed): TBLA_0B09540.
* CDS turned into pseudo (interupted by gap or chromosome end):TBLA_0C00110. 
* CDS turned into pseudo (interupted by gap or chromosome end): TBLA_0C00560.
* CDS turned into pseudo (interupted by gap or chromosome end): TBLA_0F03150.
* CDS turned into pseudo (interupted by gap or chromosome end): TBLA_0I00100.
* CDS turned into pseudo (interupted by gap or chromosome end): TBLA_0I00890.
* CDS turned into pseudo (interupted by gap or chromosome end): TBLA_0J02065.

## v1.0 (2021-05-17)

### Added

* The 10 annotated chromosomes of Tetrapisispora blattae CBS 6284 (source EBI, [GCA_000315915.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000315915.1)).
