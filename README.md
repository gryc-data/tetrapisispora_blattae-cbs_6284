## *Tetrapisispora blattae* CBS 6284

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEA70963](https://www.ebi.ac.uk/ena/browser/view/PRJEA70963)
* **Assembly accession**: [GCA_000315915.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000315915.1)
* **Original submitter**: Wolfe Laboratory (Trinity College Dublin)

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: ASM31591v1
* **Assembly length**: 14,048,593
* **#Chromosomes**: 10 
* **Mitochondiral**: No
* **N50 (L50)**: 1,449,145 (4)

### Annotation overview

* **Original annotator**: Wolfe Laboratory (Trinity College Dublin)
* **CDS count**: 5389
* **Pseudogene count**: 0
* **tRNA count**: 327
* **rRNA count**: 10
* **Mobile element count**: 0
